#!/usr/bin/env bash
#

source "$(dirname "$0")/common.sh"

# Required parameters
BOT_TOKEN=${BOT_TOKEN:?'BOT_TOKEN variable missing.'}
CHAT_ID=${CHAT_ID:?'CHAT_ID variable missing.'}
MESSAGE=${MESSAGE:?'MESSAGE variable missing.'}

# Default parameters
DEBUG=${DEBUG:="false"}

run curl https://api.telegram.org/bot${BOT_TOKEN}/getMe
if [[ `cat $output_file` = *"\"ok\":true"* ]]; then
  if [[ "${DEBUG}" == "true" ]]; then
    info "BOT_TOKEN seems to work"
  fi
else
  fail "Could not get Telegram bot info. Please check BOT_TOKEN!"
fi

run curl https://api.telegram.org/bot${BOT_TOKEN}/getChat?chat_id=$CHAT_ID
if [[ `cat $output_file` = *"\"ok\":true"* ]]; then
  if [[ "${DEBUG}" == "true" ]]; then
    info "CHAT_ID seems to be correct"
  fi
else
  fail "Could not get Telegram chat info. Please check CHAT_ID!"
fi

run curl "https://api.telegram.org/bot${BOT_TOKEN}/sendMessage?text=$MESSAGE&chat_id=$CHAT_ID&parse_mode=HTML"

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi

