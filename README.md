# Bitbucket Pipelines Pipe: Telegram Message Bot

This pipe controls a [Telegram](https://telegram.org) bot and makes the bot post a message into a [Telegram](https://telegram.org) channel.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: TamaraWeber/telegram-pipe:1.0.0
    variables:
      BOT_TOKEN: "<string>"
      CHAT_ID: "<string>"
      MESSAGE: "<string>"
      # DEBUG: "<boolean>" # Optional
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| BOT_TOKEN (*)         | The token needed to control your bot |
| CHAT_ID (*)           | The unique telegram chat-id into which the bot should post the message |
| MESSAGE (*)           | The message to post |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites
You need to create a Telegram bot, unless you already have one set up. Creating a bot is an automated process and completely free of charge. A detailed explanation how to create a telegram bot can be found here <https://core.telegram.org/bots>.

In short:
* Contact Telegram's BotFather bot via your Telegram client. Here is a direct link to start chatting with the bot: <https://t.me/botfather>. 
* Send "/start" as message to BotFather. It will respond with a list of all possible commands.
* Send "/newbot" as message to BotFather. It will start an interactive process to create your bot.
* At the end of the process you will receive a long alpha-numeric token to control your new bot. Keep this token save and set it up as **BOT_TOKEN** environment variable for this pipe.

Next step will be to add your new bot to the Telegram channel into which you want it to post messages. Add it just like an other regular user via the search function. You need to give the bot admin access rights to the channel. 

Finally you need to find out the chat-id of your channel. The easiest way is to send a message which will trigger the bot like e.g. "/help" to the chat after you added the bot to it. Now open the URL <https://api.telegram.org/bot{BOT_TOKEN}/getUpdates> in a browser of your choice. You should see the message which the bot received (i.e. the "/start" message). Along with the message you will see the chat-id which is a large negative number starting with "-100". Copy the whole number including the minus prefix. This is your **CHAT_ID** which you also need to setup as an environment variable for this pipe.



## Examples

Basic example (assuming BOT_TOKEN and CHAT_ID are configured as secure repository variables in your repository settings page):

```yaml
script:
  - pipe: TamaraWeber/telegram-pipe:1.0.0
    variables:
      BOT_TOKEN: $BOT_TOKEN
      CHAT_ID: $CHAT_ID
      MESSAGE: Hello World
```

Advanced example:
Note you can use any [Bitbucket pipeline variables](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html) in your message.

```yaml
script:
  - pipe: TamaraWeber/telegram-pipe:1.0.0
    variables:
      BOT_TOKEN: $BOT_TOKEN
      CHAT_ID: $CHAT_ID
      MESSAGE: Build <b>$BITBUCKET_BUILD_NUMBER</b> in <b>$BITBUCKET_REPO_SLUG</b> just finished successfully!
```

## Support
https://bitbucket.org/TamaraWeber/telegram-pipe/